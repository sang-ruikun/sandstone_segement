# 项目描述
本项目使用均值滤波器处理单个图像为图像添加特征,学习”Sandstone_1.tif和Sandstone_1_segment.tif，掌握实现分区的模式
用学习到的分区的模式对Sandstone_2.tif进行分区，然后把生成的结果与“正确答案”：Sandstone_2_segment.tif比对，看看准确率如何
# 运行效果
![输入图片说明](segement%E8%BF%90%E8%A1%8C%E6%95%88%E6%9E%9C.png)
![输入图片说明](create_clf%E8%BF%90%E8%A1%8C%E6%95%88%E6%9E%9C.png)
![输入图片说明](clf%E6%A8%A1%E5%9E%8B%E4%BF%9D%E5%AD%98.png)
# 功能
本项目实现了以下功能：
- 使用合适的算法学习砂岩截面图Sandstone_1.tif及其对应分区图Sandstone_1_segment.tif,生成可以用来分割新砂岩图像的分类器模型clf,pickle保存到硬盘
-读取保存的模型clf,用clf对砂岩截面图Sandstone_2.tif 及其对应分区的分区进行预测，并显示预测结果

# 依赖
本项目依赖以下库：
- numpy
- sklearn
- cv2
- matplotlib
- pickle

# 使用
1. 安装依赖库
2. 运行训练脚本

## 个人信息
- 学号: 202152320117
- 年级: 2021
- 专业: 智能科学与技术
- 班级: 一班